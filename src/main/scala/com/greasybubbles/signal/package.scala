package com.greasybubbles

package object signal {

  val π: Double = Math.PI

  case class Freq(value: Double)

  implicit class FreqOps(value: Double) {
    def Hz = Freq(value)
    def kHz = Freq(value * 1000.0)
    def MHz = Freq(value * 1000000.0)
  }

  // Angle operations
  def normaliseToPlusMinusPi(angle: Double) = {
    val a = ((angle % (2 * π)) + (2 * π)) % (2 * π)
    if (a <= Math.PI) a else a - (2 * π)
  }
}
