package com.greasybubbles.signal

import Instances._
import SignalOps._

import scalaz.syntax.all._

trait Signal[A] {
  val head: A
  def tail: Signal[A]
}

object Signal {
  def apply[A](h: A, t: () => Signal[A]): Signal[A] = new Signal[A] {
    val head: A = h
    lazy val tail: Signal[A] = t()
  }

  def take[A](snl: Signal[A])(n: Int): Seq[A] = {
    @annotation.tailrec
    def go(accu: Vector[A], snl: Signal[A], n: Int): Vector[A] = {
      if (n <= 0) accu else
        go(accu :+ snl.head, snl.tail, n - 1)
    }
    go(Vector.empty, snl, n)
  }

  @annotation.tailrec
  final def drop[A](snl: Signal[A])(n: Int): Signal[A] =
    if (n <= 0) snl else drop(snl.tail)(n - 1)

  def constant[A](value: A): Signal[A] = Signal[A](value, () => constant(value))

  final def scan[S, A](snl: Signal[A])(init: S)(f: (S, A) => S): Signal[S] = {
    val newHead = f(init, snl.head)
    Signal[S](newHead, () => scan(snl.tail)(newHead)(f))
  }

  def zip[A, B](snl1: Signal[A], snl2: Signal[B]) = (snl1 |@| snl2)((_, _))

  def zipWith[A, B, C](snl1: Signal[A], snl2: Signal[B])(f: (A, B) => C): Signal[C] = Signal[C](
    h = f(snl1.head, snl2.head),
    t = () => zipWith(snl1.tail, snl2.tail)(f)
  )

  def zipWith3[A, B, C, D](a: Signal[A], b: Signal[B], c: Signal[C])(f: (A, B, C) => D): Signal[D] = Signal[D](
    h = f(a.head, b.head, c.head),
    t = () => zipWith3(a.tail, b.tail, c.tail)(f)
  )
}