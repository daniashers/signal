package com.greasybubbles.signal

import Instances._
import SignalOps._

import scalaz.syntax.all._

object ColourTv extends scalax.chart.module.Charting {

  val sampleRate = 16.MHz
  val ChromaFrequency: Freq = 4.43361875.MHz
  val linePeriod = 1024
  val Vpp = 190.0 // peak-to-peak input amplitude

  def decode(input: Signal[Double]): Signal[Pixel] = {

    val video = Tools.clamp(input).map(_ / Vpp) // normalise to range [0, 1]

    val isSync: Signal[Boolean] = video.cobind(_.take(5).forall(_ < 0.1))
    val hPos: Signal[Long] = Tools.timer(reset = isSync)
    val vPos: Signal[Long] = Tools.count(isSync.posEdge)
    val isColourBurst: Signal[Boolean] = hPos.map(h => (h > 20) && (h < 50))

    val blackLevel = Tools.hold(video.drop(130).avg(10), trigger = isSync.posEdge)

    val composite = (video |@| blackLevel) { case (x, bl) => (x - bl) / 0.7 }

    val luma   = composite.biquadFilter(Filters.chromaNotch)
    val chroma = composite.biquadFilter(Filters.chromaBandpass).drop(4)

    val referencePhase: Signal[Double] =
      Tools.pll(sampleRate, Pal.ChromaFrequency, chroma, trigger = isColourBurst.negEdge)

    val (u0, v0): (Signal[Double], Signal[Double]) =
      Pal.chromaDemod(sampleRate, referencePhase, chroma)

    val u = (u0 |@| u0.drop(linePeriod)) { case (c, d) => (c + d) / 2 }
    val v = (v0 |@| v0.drop(linePeriod)) { case (c, d) => (c + d) / 2 }

    val saturationControl = 2.5
    val r = (luma |@| u |@| v) { case (y, _, v) => y +              2.03 * v  * saturationControl }
    val g = (luma |@| u |@| v) { case (y, u, v) => y + (-.394 * u - .581 * v) * saturationControl }
    val b = (luma |@| u |@| v) { case (y, u, _) => y +   1.14 * u             * saturationControl }

    (hPos |@| vPos |@| r |@| g |@| b)(Pixel.apply)
  }
}