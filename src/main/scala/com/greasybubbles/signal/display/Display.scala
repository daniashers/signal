package com.greasybubbles.signal.display

import java.awt._
import java.awt.image._
import javax.swing._

class Display(width: Int, height: Int, hStretch: Double, vStretch: Double) extends JPanel {

  setPreferredSize(new Dimension((width * hStretch).toInt, (height * vStretch).toInt))

  private val image: BufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
  private lazy val raster: WritableRaster = image.getRaster

  def putPixel(x: Int, y: Int, r: Double, g: Double, b: Double) = {
    for {
      u <- ((x * 1) until ((x + 1) * 1)).toList.filter(_ < width * 1)
      v <- ((y * 1) until ((y + 1) * 1)).toList.filter(_ < height * 1)
    } yield {
      raster.setPixel(u, v, Array[Int](
        (r * 255).toInt.max(0).min(255),
        (g * 255).toInt.max(0).min(255),
        (b * 255).toInt.max(0).min(255)
      ))
    }
    this.repaint()
  }

  override def paintComponent(g: Graphics): Unit = {
    g.drawImage(image, 0, 0, getWidth, getHeight, null)
  }
}

object Display {
  def start(width: Int, height: Int, hStretch: Double, vStretch: Double): Display = {
    val display: Display = new Display(width, height, hStretch, vStretch)
    EventQueue.invokeLater(new Runnable() {
      override def run: Unit = {
        val f: JFrame = new JFrame()
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        //f.setDefaultLookAndFeelDecorated(true)
        f.setResizable(false)
        f.add(display, BorderLayout.CENTER)
        f.pack()
        f.setVisible(true)
      }
    })
    display
  }
}

//object DisplayTest extends App {
//
//  val display = Display.bootstrap(1600, 1200, 1, 1)
//
//  (0 until 312).foreach { i =>
//    display.putPixel(i, i, 1.0, 1.0, 1.0)
//  }
//
//  while (true) {}
//}