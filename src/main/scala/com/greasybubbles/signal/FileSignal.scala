package com.greasybubbles.signal

import Instances._
import scalaz.syntax.comonad._

import java.io.{File, FileInputStream}

object FileSignal {
  def open(filename: String): Signal[Byte] = {
    val file = new File(filename)
    val fis: FileInputStream = new FileInputStream(file)
    val fileLength = file.length
    val data: Array[Byte] = new Array[Byte](fileLength.toInt)
    fis.read(data)
    fis.close()
    ArraySignal(data, 0)
  }

  def openUnsigned(filename: String): Signal[Int] = {
    open(filename).map { sbyte =>
      if (sbyte >= 0) sbyte
      else 256 + sbyte
    }
  }
}
