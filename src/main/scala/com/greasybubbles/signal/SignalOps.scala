package com.greasybubbles.signal

import com.greasybubbles.signal.Instances._

import scalaz.syntax.comonad._


object SignalOps {
  implicit class GenericOps[A](snl: Signal[A]) {
    def take(n: Int): Seq[A] = Signal.take(snl)(n)
    def drop(n: Int): Signal[A] = Signal.drop(snl)(n)
    def scan[S](init: S)(f: (S, A) => S): Signal[S] = Signal.scan(snl)(init)(f)
  }

  implicit class BoolOps(snl: Signal[Boolean]) {
    def posEdge: Signal[Boolean] = snl.cobind[Boolean](value => !value.head &&  value.tail.head)
    def negEdge: Signal[Boolean] = snl.cobind[Boolean](value =>  value.head && !value.tail.head)
  }

  implicit class DoubleOps(snl: Signal[Double]) {
    def avg(n: Int) = snl.cobind(_.take(n).sum/n)
    def biquadFilter(biquadDef: Biquad): Signal[Double] = Biquad.applyTransposed2(biquadDef)(snl)
    def fir(coefficients: Seq[Double]): Signal[Double] = snl.cobind { s =>
      Signal.take(s)(coefficients.length).zip(coefficients).map{ case (a, b) => a * b }.sum
    }
  }
}