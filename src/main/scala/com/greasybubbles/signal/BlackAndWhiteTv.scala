package com.greasybubbles.signal

import Instances._
import SignalOps._

import scalaz.syntax.all._

object BlackAndWhiteTv {

  val Vpp = 190.0 // peak-to-peak input amplitude

  def decode(signal: Signal[Double]): Signal[Pixel] = {

    val video = Tools.clamp(signal).map(_ / Vpp) // normalise to range [0, 1]

    val isSync: Signal[Boolean] = video.cobind(_.take(5).forall(_ < 0.1))
    val hPos: Signal[Long] = Tools.timer(reset = isSync)
    val vPos: Signal[Long] = Tools.count(condition = isSync.posEdge)

    val blackLevel: Signal[Double] = Tools.hold(video.drop(130).avg(10), trigger = isSync.posEdge)

    val brightness: Signal[Double] = (video |@| blackLevel) { case (v, b) =>
      (v - b) / 0.7
    }

    (hPos |@| vPos |@| brightness) { case (x, y, v) => Pixel.grey(x, y, v) }
  }
}