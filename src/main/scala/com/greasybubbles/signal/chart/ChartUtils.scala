package com.greasybubbles.signal.chart

import com.greasybubbles.signal.Signal
import com.greasybubbles.signal.SignalOps._

object ChartUtils extends scalax.chart.module.Charting  {
  def chartSignals(length: Int)(signals: (String, Signal[Double])*): Unit = {
    val t0 = System.nanoTime()
    val data = signals.toList.map { case (label, snl) =>
      label -> snl.take(length).zipWithIndex.map(_.swap)
    }
    val t1 = System.nanoTime()
    val seconds = (t1 - t0).toDouble / 1000000000.0
    println(s"Chart data generated in $seconds seconds." )
    XYLineChart(data).show()
  }
}