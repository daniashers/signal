package com.greasybubbles.signal

case class Pixel(x: Long, y: Long, r: Double, g: Double, b: Double)

object Pixel {
  def grey(x: Long, y: Long, v: Double): Pixel = Pixel(x, y, v, v, v)
}