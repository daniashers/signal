package com.greasybubbles.signal

import Instances._
import scalaz.syntax.all._
import SignalOps._

object Tools {

  def movingAverage(snl: Signal[Double])(n: Int) = snl.cobind(_.take(n).sum/n)

  /**
    * Construct a signal that increases by one whenever the `condition` signal is true
    * and remains unchanged otherwise.
    */
  def count(condition: Signal[Boolean]): Signal[Long] = {
    condition.scan[Long](0)((count, cond) => if (cond) count + 1 else count)
  }

  /**
    * Construct a signal that counts upwards at every tick,
    * but resets to zero if the `reset` signal becomes true.
    */
  def timer(reset: Signal[Boolean]): Signal[Long] = {
    reset.scan[Long](0)((elapsed, reset) => if (reset) 0 else elapsed + 1)
  }

  /**
    * When 'trigger' is true, memorises and holds the value that 'input' had in that moment
    * until 'trigger' becomes true again.
    */
  def hold[A](input: => Signal[A], trigger: Signal[Boolean]): Signal[A] = {
    Signal.zip(input, trigger).scan[A](input.head) { case (last, (in, trig)) =>
      if (trig) in else last
    }
  }

  /**
    * Generates a ramp from 0 to 2π with the given period
    */
  def oscillator(period: Double): Signal[Double] = {
    val Δφ = 2 * π / period
    Signal.constant(Δφ).scan(0.0) { case (φ, step) => (φ + step) % (2 * π) }
  }

  /**
    * Code equivalent of a positive clamper circuit.
    * Bring the AC-coupled input signal back to a positive range [0, ...] by tracking the minimum level
    * of the signal and shifting accordingly. The parameters used are determined experimentally!!
    */
  def clamp(s: Signal[Double]): Signal[Double] = {
    // Keeps track of the minimum value encountered, and it slowly decays to zero if not refreshed with a new minimum.
    val decay = 0.0001
    val minimum =
    s.scan[Double](0.0) { case (min, value) => if (value < min) value else min * Math.exp(-decay)}
    (s |@| minimum)(_ - _)
  }

  /**
    * Compares the phase of a sinusoidal input signal against a reference phase ramp signal (periodic, linear 0-2π sweeps)
    */
  def phaseCompare(input: Signal[Double], reference: Signal[Double]): Signal[Double] = {
    lazy val (i, q) = qamDemod(reference, input)
    (i |@| q)(Math.atan2)
  }

  /**
    * Outputs a phase ramp signal (periodic, linear 0-2π sweeps) whose phase can be locked to that of a
    * reference sinusoidal signal of the same frequency. The 'trigger' signal controls when this synchronisation
    * is performed. When 'trigger' is false the 'reference' signal is not required to be present.
    * If the reference signal does not exist when triggered or doesn't have the same frequency specified
    * in the arguments, behaviour is undefined.
    */
  def pll(sampleRate: Freq, frequency: Freq, input: Signal[Double], trigger: Signal[Boolean]): Signal[Double] = {
    val freeOscillator = Tools.oscillator(sampleRate.value / frequency.value)

    val phaseCorrection = Tools.hold[Double](phaseCompare(input, freeOscillator), trigger)

    (freeOscillator |@| phaseCorrection) { case (free, diff) =>
      free - diff
    }
  }

  /**
    * Demodulates a quadrature amplitude modulated signal using 'ref' as the phase reference.
    * 'ref' should be in the form of a phase ramp (periodic, linear 0-2π sweeps).
    * Outputs in-phase and quadrature demodulated signals.
    * The output signals are then limited to a bandwidth of 1.3 MHz (for a 16 MHz sample rate).
    */
  def qamDemod(ref: Signal[Double], input: Signal[Double]): (Signal[Double], Signal[Double]) = {
    def multiply(s1: Signal[Double], s2: Signal[Double]): Signal[Double] = (s1 |@| s2)(_ * _)
    val iRef: Signal[Double] = ref.map(Math.sin)
    val qRef: Signal[Double] = ref.map(Math.cos)
    val i = multiply(iRef, input).biquadFilter(Filters.uvLowPass)
    val q = multiply(qRef, input).biquadFilter(Filters.uvLowPass)
    (i, q)
  }
}
