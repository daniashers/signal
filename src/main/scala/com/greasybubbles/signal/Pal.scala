package com.greasybubbles.signal

import Instances._
import scalaz.syntax.all._
import SignalOps._

object Pal {

  val ChromaFrequency: Freq = 4.43361875.MHz

  /**
    * @param reference a 0-2π ramp locked to the burst phase
    * @param chroma the chrominance signal
    * @return a signal of tuples containing the U and V signals
    */
  def chromaDemod(sampleRate: Freq, reference: Signal[Double], chroma: Signal[Double]): (Signal[Double], Signal[Double]) = {

    val isAlt = isAltLine(sampleRate, reference)

    val trueReference = (reference |@| isAlt) { case (ref, isAlt) =>
      if (isAlt) ref + π/4 else ref - π/4
    }

    val (i0, q) = Tools.qamDemod(trueReference, chroma)
    val i = (i0 |@| isAlt) { case (i0, isAlt) =>
      if (isAlt) (-i0) else i0
    }
    (i, q)
  }

  /**
    * Based on the phase difference of the reference signal between one line and the following,
    * determine whether the given line is to be decoded as 'normal' or 'phase alternated' (V axis reversed).
    * A line is to be considered 'alternate' if the reference signal has jumped plus 90 degrees compared to
    * the previous line, and 'normal' if it has jumped minus 90 degrees.
    */
  def isAltLine(sampleRate: Freq, reference: Signal[Double]): Signal[Boolean] = {
    val phaseShift = reference.cobind { s =>
      normaliseToPlusMinusPi(s.tail.head - s.head - 2 * π * ChromaFrequency.value / sampleRate.value)
    }
    phaseShift.scan(false) { (isInv, phaseShift) =>
      if (Math.abs(phaseShift) < π / 8) isInv
      else if (phaseShift > 0) true
      else false
    }
  }
}