package com.greasybubbles.signal

case class ArraySignal[A](values: Seq[A], default: A, cursor: Int = 0) extends Signal[A] {
  val head: A = if (cursor < values.length) values(cursor) else default
  lazy val tail: Signal[A] = if (cursor < values.length) this.copy(cursor = cursor + 1) else Signal.constant(default)
}
