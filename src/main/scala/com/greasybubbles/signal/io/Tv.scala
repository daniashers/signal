package com.greasybubbles.signal.io

import com.greasybubbles.signal.Instances._
import com.greasybubbles.signal.{FileSignal, Pixel, Signal}
import com.greasybubbles.signal.display.Display
import com.greasybubbles.signal.SignalOps._

import scalaz.syntax.all._

object Tv {
  def go(fileName: String, decode: Signal[Double] => Signal[Pixel]): Unit = {
    lazy val display: Display = Display.start(1024, 312, hStretch = 1.0, vStretch = 2.25)
    val input: Signal[Double] = FileSignal.openUnsigned(fileName).map(_.toDouble - 128.0) // centre around zero
    val decoded: Signal[Pixel] = decode(input)
    decoded.map { case Pixel(x, y, r, g, b) =>
      display.putPixel(x.toInt, y.toInt, r, g, b)
    }.take(320000)
  }
}