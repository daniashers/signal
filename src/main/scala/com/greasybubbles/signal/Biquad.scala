package com.greasybubbles.signal

import Instances._
import scalaz.syntax.all._
import SignalOps._

case class Biquad(a0: Double, a1: Double, a2: Double, b1: Double, b2: Double, gainAdj: Double)

object Biquad {

  val Noop: Biquad = Biquad(1, 0, 0, 0, 0, 1.0)

  /**
    * A Direct Form I biquad implementation.
    * Straightforward to understand but sometimes unstable.
    */
  def applyDirect1(q: Biquad)(in: Signal[Double]): Signal[Double] = {
    case class D1State(x1: Double, x2: Double, y1: Double, y2: Double)
    val EmptyState: D1State = D1State(0, 0, 0, 0)
    def f(s: D1State, x: Double): D1State = {
      val b =
        q.a0 *   x  +
        q.a1 * s.x1 +
        q.a2 * s.x2 +
        q.b1 * s.y1 +
        q.b2 * s.y2
      D1State(
        x1 = x,
        x2 = s.x1,
        y1 = b,
        y2 = s.y1
      )
    }
    in
      .scan[D1State](EmptyState)(f)
      .map(_.y1 * q.gainAdj)
  }

  /**
    * A Transposed Form II biquad implementation.
    * More likely to be stable.
    */
  def applyTransposed2(q: Biquad)(in: Signal[Double]): Signal[Double] = {
    case class T2State(y: Double, s1: Double, s2: Double)
    val EmptyState = T2State(0.0, 0.0, 0.0)
    def f(s: T2State, x: Double): T2State = {
      val y = (q.a0 * x) + s.s1
      val s1 = s.s2 + (q.a1 * x) - (q.b1 * y)
      val s2 = (q.a2 * x) - (q.b2 * y)
      T2State(y, s1, s2)
    }
    in
      .scan[T2State](EmptyState)(f)
      .map(_.y * q.gainAdj)
  }
}

