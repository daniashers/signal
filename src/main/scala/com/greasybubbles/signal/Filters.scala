package com.greasybubbles.signal

/**
  * Please note: All the following filters assume a sample rate of 16 MHz.
  */
object Filters {
  val chromaNotch = Biquad(
    a0 = 0.5893142550411007,
    a1 = 0.19972179524567452,
    a2 = 0.5893142550411007,
    b1 = 0.19972179524567452,
    b2 = 0.1786285100822014,
    gainAdj = 1.0
  )

  val chromaBandpass = Biquad(
    a0 = 0.41068574495889937,
    a1 = 0,
    a2 = -0.41068574495889937,
    b1 = 0.19972179524567452,
    b2 = 0.1786285100822014,
    gainAdj = 1.0
  )

  // 1.3 MHz lowpass filter for the two colour components after demodulation
  val uvLowPass = Biquad(
    a0 = 0.047379541988403,
    a1 = 0.094759083976806,
    a2 = 0.047379541988403,
    b1 = -1.2968522868274828,
    b2 = 0.48637045478109475,
    gainAdj = 1.0
  )

  val firLowPass = Seq(0.10722524120930188,
    0.061572058135216934,
    0.07344261886228934,
    0.08207920358888718,
    0.0867216436768284,
    0.0867216436768284,
    0.08207920358888718,
    0.07344261886228934,
    0.061572058135216934,
    0.10722524120930188)
}
