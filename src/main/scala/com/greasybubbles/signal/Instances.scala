package com.greasybubbles.signal

import scalaz.{Applicative, Comonad}
import scalaz.syntax.all._


object Instances {

  implicit object SignalInstances extends Comonad[Signal] with Applicative[Signal] {
    def copoint[A](p: Signal[A]): A = p.head

    def cobind[A, B](fa: Signal[A])(f: Signal[A] => B): Signal[B] = Signal[B](f(fa), () => cobind(fa.tail)(f))

    def point[A](a: => A): Signal[A] = Signal.constant(a)

    override def map[A, B](fa: Signal[A])(f: A => B): Signal[B] = Signal[B](f(fa.head), () => map(fa.tail)(f))

    def ap[A, B](fa: => Signal[A])(f: => Signal[A => B]): Signal[B] = Signal.zipWith(fa, f) { case (a, fn) => fn(a) }

    // overriding this for performance reasons
    override def apply2[A, B, C](fa: => Signal[A], fb: => Signal[B])(f: (A, B) => C): Signal[C] = Signal.zipWith(fa, fb)(f)
    override def apply3[A, B, C, D](fa: => Signal[A], fb: => Signal[B], fc: => Signal[C])(f: (A, B, C) => D): Signal[D] = Signal.zipWith3(fa, fb, fc)(f)
  }
}