package com.greasybubbles.signal

import com.greasybubbles.signal.io.Tv

object Monochrome extends App {
  val fileName = "/Users/dani/Desktop/cobind-and-chill/moore+.snl"
  Tv.go(fileName, BlackAndWhiteTv.decode)
}

object Colour extends App {
  val fileName = "/Users/dani/Desktop/cobind-and-chill/pm5544+.snl"
  Tv.go(fileName, ColourTv.decode)
}