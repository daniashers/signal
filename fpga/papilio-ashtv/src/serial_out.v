`include "definitions.vh"

module ram_to_serial_reader (
  input           clk,
  input           trigger,
  input           interrupt,
  output reg      busy,
  // Memory signals
  output reg [`ADDRESS_LINES-1:0] mem_addr,
  input [7:0]                     mem_data,
  output reg                      mem_re, // read enable
  // Signals for communicating with main memory
  output reg       serial_trigger,
  output [7:0]     serial_data,
  input            serial_is_busy
);

  posedge_detector a_posedge_detector (
    .signal_in(trigger),
    .clk(clk),
    .is_edge(trigger_pulse)
  );

  // Values to avoid confusion
  `define ENABLE  0 // RAM read enable is active low
  `define DISABLE 1

  assign serial_data = mem_data; // real version
  //assign serial_data = mem_addr[7:0]; // debug version

  initial begin
    mem_re = `DISABLE;
		mem_addr <= `ADDR_HIGH_Z;
		serial_trigger <= 0;
		busy <= 0;
  end

  always @ (posedge clk) begin
    case (busy)
      0: begin // we are not busy
        mem_re = `DISABLE;
        mem_addr <= `ADDR_HIGH_Z; // set address outputs to high impedance if not using
        if (trigger_pulse) begin
          // we have been triggered! begin the readback process
          busy <= 1;
          mem_addr <= 0;
          mem_re = `ENABLE;
          serial_trigger <= 1;
        end
      end
      1: begin // we are busy, i.e. in the middle of a readback process
        case (serial_is_busy)
          0: begin // serial tranfer is not busy
            if (!serial_trigger) begin
              mem_addr <= mem_addr + 1'b1; // increase the RAM address
              if ((mem_addr == `MAX_ADDR) || interrupt)
                busy <= 0; // if we have readback all the RAM, end here
              else
                serial_trigger <= 1; // otherwise, trigger the transfer of the next byte
            end
          end
          1: begin
            serial_trigger <= 0; // if the UART has triggered, bring the trigger back to 0
          end
        endcase
      end
    endcase
  end
endmodule
