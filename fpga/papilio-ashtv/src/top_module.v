`include "definitions.vh"

module ashtv_papilio (
  // 100MHz system clock
  input   CLK,
  output  DUO_LED,
  input   DUO_SW1,
  output  TXD, //serial_tx,
  
  // SRAM signals
  output        sram_ce, // chip enable, active low
  output        sram_oe, // read enable, active low
  output        sram_we, // write enable, active low
  output [20:0] sram_addr,
  inout  [7:0]  sram_data,

  // ADC signals
  output        adc_clk,
  input [7:0]   adc_data,
  
  // Misc
  output        ARD_RESET // Reset signal for the AVR (Arduino) - active low
);

  assign ARD_RESET = 1'b0; // keep the AVR (Arduino) in a reset state
  assign sram_ce = 1'b0; // always have the chip active

  assign adc_clk = CLK;

  // Status LED
  assign DUO_LED = acquire_is_busy | dumper_is_busy;

  wire serial_trigger;
  wire serial_is_busy;
  wire [7:0] serial_data_from_memory;
  wire dumper_is_busy;
  wire acquire_is_busy;

  debouncer a_debouncer (
    .clk(CLK),
    .bouncy(DUO_SW1),
    .debounced(trigger_capture)
  );

  // Reads data from the ADC and stores it in RAM.
  ram_signal_writer a_ram_signal_writer (
    .clk(CLK),
    .trigger(trigger_capture),
    .busy(acquire_is_busy),
    .adc_data(adc_data),
	  .mem_addr(sram_addr),
    .mem_we(sram_we),
    .mem_data(sram_data)
  );
  
  ram_to_serial_reader a_ram_to_serial_reader (
    .clk(CLK),
    .trigger(~acquire_is_busy),
    .interrupt(1'b0),
    .busy(dumper_is_busy),
    // Signals for communicating with main memory
    .mem_addr(sram_addr),
    .mem_data(sram_data),
	  .mem_re(sram_oe),
    // Signals for communicating with the serial marshaller
    .serial_trigger(serial_trigger),
    .serial_data(serial_data_from_memory),
    .serial_is_busy(serial_is_busy)
  );

  // The Serial transmitter
  assign adc_clk = CLK;
  uart_sender a_uart_sender (
    .clk(CLK),
    .data_to_transmit(serial_data_from_memory), // real version
    //.data_to_transmit(8'b00110101), // debug version
    .tx_out(TXD),
    .busy(serial_is_busy),
    .trigger(serial_trigger) // real version
    //.trigger(1'b1) // debug version
  );

endmodule
