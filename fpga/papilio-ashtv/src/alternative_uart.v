`timescale 1ns / 1ps

module alternative_uart (
    input clk,
	 input trigger, // start transmitting the current byte
    input [7:0] data_to_transmit,
	 output tx_out,
	 output reg busy // high if transmission is in progress
    );

reg [31:0] divider_counter;
reg serial_output;

reg [7:0] byte_to_transmit;
reg [8:0] shift_register; // used to process the bits one at a time
reg [3:0] bits_left; // how many bits (including sync bits) are left to transmit

assign tx_out = serial_output;

initial begin
    divider_counter <= 0;
	 bits_left <= 0;
	 busy <= 0;
end

`define SYSTEM_CLOCK_HZ 32_000_000
`define BAUD_RATE       115200
`define DIVIDER         `SYSTEM_CLOCK_HZ / `BAUD_RATE

always @(posedge clk) begin
	// if we have been triggered...
	if ((~busy) && trigger) begin
	  byte_to_transmit <= data_to_transmit;
	  shift_register <= { byte_to_transmit[7:0], 1'h0 };
	  bits_left <= (1 + 8 + 2); // 1 start, 8 data, 2 stop
	  busy <= 1;
	end

	 // create the divided frequency for the serial clock
    divider_counter <= divider_counter + 1;
	 if (divider_counter == `DIVIDER) begin
		divider_counter <= 0;
		if (busy) begin
			// do the bit-banging
		   { shift_register, serial_output } <= { 1'h1, shift_register }; // shift right through output wire
		   bits_left <= bits_left - 1;
			if (bits_left == 0) begin
				busy <= 0;
			end
		end
	 end
end

endmodule
