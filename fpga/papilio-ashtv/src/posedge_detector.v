module posedge_detector (
  input signal_in,
  input clk,
  output is_edge
);

  reg delayed;

  always @ (posedge clk) begin
    delayed <= signal_in;
  end

  assign is_edge = signal_in & ~delayed;            
endmodule 
