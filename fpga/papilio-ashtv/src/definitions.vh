`define ADDRESS_LINES  21 // 21 lines required to address 2 MB
`define MAX_ADDR       21'b01011_11111111_11111111
`define ADDR_HIGH_Z    21'bzzzzz_zzzzzzzz_zzzzzzzz
`define DATA_HIGH_Z    8'bzzzzzzzz

`define SYSTEM_CLOCK_HZ 32_000_000
`define BAUD_RATE       57600
