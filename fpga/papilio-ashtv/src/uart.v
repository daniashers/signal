`include "definitions.vh"

module uart_sender(
  input clk,
  input trigger, // start transmitting the current byte
  input [7:0] data_to_transmit,
  output reg tx_out,
  output reg busy // high if transmission is in progress
);

  posedge_detector a_posedge_detector (
    .signal_in(trigger),
    .clk(clk),
    .is_edge(trigger_pulse)
  );

  reg [11:0] divider_counter;

  reg [7:0] data_buffer;
  reg [3:0] bits_transmitted; // how many bits (including sync bits) have been sent

  initial begin
    divider_counter <= 0;
    bits_transmitted <= 0;
    busy <= 0;
  end

  `define DIVIDER         `SYSTEM_CLOCK_HZ / `BAUD_RATE

  always @(posedge clk) begin
    // if we have been triggered...
    case (busy)
      0: begin // if we are not busy sending a byte...
        tx_out <= 1;
        if (trigger) begin // check for trigger
          busy <= 1;
          data_buffer <= data_to_transmit;
          bits_transmitted <= 0;
          divider_counter <= 0;
        end
      end
      1: begin // if we are busy...
        // Perhaps not the most efficient implementation but simple to read.
        case (bits_transmitted)
          0: tx_out <= 1'b0;           // Start bit
          1: tx_out <= data_buffer[0]; // lsb first
          2: tx_out <= data_buffer[1];
          3: tx_out <= data_buffer[2];
          4: tx_out <= data_buffer[3];
          5: tx_out <= data_buffer[4];
          6: tx_out <= data_buffer[5];
          7: tx_out <= data_buffer[6];
          8: tx_out <= data_buffer[7]; // msb last
          9: tx_out <= 1'b1;           // Stop bit
          10: busy <= 0;
        endcase
        divider_counter <= divider_counter + 1'b1;
        if (divider_counter == `DIVIDER) begin
          divider_counter <= 0;
          bits_transmitted <= bits_transmitted + 1'b1;
        end
      end
    endcase
  end
endmodule
