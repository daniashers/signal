module debouncer (
  input      clk,      // ~50MHz clock
  input      bouncy,   // input to be debounced
  output reg debounced // debounced output
);

  // Synchronize the switch input to the clock
  reg PB_sync_0;
  always @(posedge clk) 
    PB_sync_0 <= bouncy;

  reg synced_bouncy;
  always @(posedge clk) 
    synced_bouncy <= PB_sync_0;

  // Debounce the switch
  reg [15:0] counter;
  always @(posedge clk)
    if (debounced == synced_bouncy)
      counter <= 0;
    else begin
      counter <= counter + 1'b1; 
      if (counter == 16'hffff) 
        debounced <= ~debounced; 
    end

endmodule
