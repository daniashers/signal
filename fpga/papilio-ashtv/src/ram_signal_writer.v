`include "definitions.vh"

module ram_signal_writer (
  input            clk,
  input            trigger, // starts the capture
  output reg       busy,
  input  [7:0]     adc_data,
  // Memory signals:
  output reg [`ADDRESS_LINES-1:0] mem_addr,
  output reg [7:0]                mem_data,
  output reg                      mem_we // write enable, active low
);

  posedge_detector a_posedge_detector (
    .signal_in(trigger),
    .clk(clk),
    .is_edge(trigger_pulse)
  );

  // Values to avoid confusion
  `define ENABLE  0 // RAM write enable is active low
  `define DISABLE 1

  // Initialize registers
  initial begin
    busy <= 0;
    mem_we <= `DISABLE;
    mem_addr <= `ADDR_HIGH_Z;
    mem_data <= `DATA_HIGH_Z;
  end

  reg divider; // we only need 1 bit to divide, as we only want to divide by 2

  always @ (posedge clk) begin
    if (!busy) begin
      mem_we <= `DISABLE;
      mem_addr <= `ADDR_HIGH_Z; // set outputs to high impedance if not using
      mem_data <= `DATA_HIGH_Z; // set outputs to high impedance if not using
      if (trigger_pulse) begin
        busy <= 1;
        mem_addr <= 0;
        divider <= 0;
      end
    end else begin
      case (divider)
        0: begin // end the write pulse (if started), set up the address and the data
          mem_we <= `DISABLE;
          if (mem_addr == `MAX_ADDR) begin
            busy <= 0;
            mem_addr <= `ADDR_HIGH_Z;
            mem_data <= `DATA_HIGH_Z;
          end else begin
            mem_data <= adc_data; // real version
            mem_addr <= mem_addr + 1'b1;
            //mem_data <= mem_addr[7:0]; // debug version
          end
        end
        1: begin // start the write pulse that will write the data
          mem_we <= `ENABLE;               
        end
      endcase
      divider <= divider + 1'b1;
    end
  end

endmodule
