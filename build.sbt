name := "signal"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.27"
libraryDependencies += "com.github.wookietreiber" %% "scala-chart" % "latest.integration"