#Signal
Project using Comonad and other functional abstractions to decode an analogue PAL television picture.
As currently configured, this project consumes files of sampled video data at 16 MHz, 8-bit unsigned. 
No such files are stored in the repository due to their size; contact the author if interested.
